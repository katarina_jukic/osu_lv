

list=[]
while True:
    number=input("Upiši broj: ")
    if number=="Done":
        break
    elif number.isnumeric() == False:
        print("Niste unjeli broj u listu")
    else:
        list.append(int(number))

print("Ukupni broj unesenih brojeva je: ", len(list))
print("Prosjecan broj u nizu: ", sum(list)/len(list))
print("Minimalan broj u nizu: ", min(list))
print("Maksimalan broj u nizu: ", max(list))


