file = open ('SMSSpamCollection.txt')

spam=0
ham=0
s_words=0
h_words=0

s_exclamation=0

for line in file:
    line=line.rstrip()
    words=line.split()
    if words[0]=="spam":
        spam=spam+1
        s_words=s_words+len(words)
    elif words[0]=="ham":
        ham=ham+1
        h_words=h_words+len(words)
    if words[-1].endswith("!"):
        s_exclamation=s_exclamation+1

file.close()
print("Average for spam is: ",s_words/spam)
print("Average for ham is: ",h_words/ham)
print("Message that is spam and ends with exclamation count is: ",s_exclamation)

