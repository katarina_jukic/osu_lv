try:
    grade=float(input("Upiši ocjenu: "))
    if grade>1.0 or grade<0.0:
        print("Broj nije između 0.0 i 0.1")
    elif grade>=0.9:
        print("A")
    elif grade>=0.8:
        print("B")
    elif grade>=0.7:
        print("C")
    elif grade>=0.6:
        print("D")
    else:
        print("F")


except ValueError:
    print("Broj nije upisan")