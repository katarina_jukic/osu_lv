import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.base import accuracy_score

from sklearn.datasets import make_classification
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.metrics import accuracy_score, precision_score, recall_score, confusion_matrix
from sklearn.model_selection import train_test_split


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                           random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=5)

# a
xmin, xmax = -5, 5
ymin, ymax = -5, 5

xd = np.array([xmin, xmax])
yd = np.array([ymin, ymax])

plt.plot(xd, yd, 'k', lw=1, ls='-')
plt.fill_between(xd, yd, ymin, color='red', alpha=0.2)
plt.fill_between(xd, yd, ymax, color='blue', alpha=0.2)
plt.scatter(X_train[:, 0], X_train[:, 1], c='r', label='podaci za treniranje')
plt.scatter(X_test[:, 0], X_test[:, 1], c='b',
            marker='*', label='podaci za testiranje')

plt.xlabel('x1')
plt.ylabel('x2')
plt.legend()

plt.show()

# b
LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train, y_train)

# c
coef = LogRegression_model.coef_[0]
intercept = LogRegression_model.intercept_

x1 = np.linspace(-5, 5, 100)
x2 = -(intercept + coef[0]*x1) / coef[1]

plt.plot(x1, x2, color='red', linestyle='-')
plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap='bwr')
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()


# d
y_pred = LogRegression_model.predict(X_test)
# tocnost
print(" Tocnost : ", accuracy_score(y_test, y_pred))
print(" Preciznost : ", precision_score(y_test, y_pred))
print(" Odziv : ", recall_score(y_test, y_pred))
# matrica zabune
cm = confusion_matrix(y_test, y_pred)
print(" Matrica zabune : ", cm)
disp = ConfusionMatrixDisplay(confusion_matrix=cm)
disp.plot()
plt.show()

#e
colorsEvaluation=['red', 'green']
plt.scatter(X_test[:,0], X_test[:,1], c=y_test==y_pred, cmap=matplotlib.colors.ListedColormap(colorsEvaluation))
plt.xlabel('x1')
plt.ylabel('x2')
cbar=plt.colorbar(ticks=[0,1])
cbar.ax.set_yticklabels(['False','True'])
plt.show()

