import numpy as np
import matplotlib.pyplot as plt

img=plt.imread('road.jpg')

atask_img=np.clip(img + 0.5, 0, 1)
plt.figure()
plt.imshow(atask_img)
plt.show()

btask_img=img[:, int(img.shape[1]/4):int(img.shape[1]/2)]
plt.figure()
plt.imshow(btask_img)
plt.show()

ctask_img = np.rot90(img, k=1)
plt.figure()
plt.imshow(ctask_img)
plt.show()


dtask_img = np.flip(img, axis=1)
plt.figure()
plt.imshow(dtask_img)
plt.show()

