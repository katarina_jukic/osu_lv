import numpy as np
import matplotlib . pyplot as plt

bijelo=np.zeros((50,50))
crno=np.ones((50,50))*255
stupac_1=np.vstack((crno,bijelo))
stupac_2=np.vstack((bijelo,crno))
img=np.hstack((stupac_1,stupac_2))

plt.figure()
plt.imshow(img,cmap='gray')
plt.show()
