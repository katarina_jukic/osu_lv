import numpy as np
import matplotlib . pyplot as plt

file=np.genfromtxt("data.csv", delimiter=',',skip_header=1)

print(len(file))

visina=file[:,1]
tezina=file[:,2]
plt.scatter(visina,tezina,marker ="*",s=10)
plt . xlabel ('visina')
plt . ylabel ('tezina')
plt.title ('b')
plt.show ()

visina_50=file[::50,1]
tezina_50=file[::50,2]
plt.scatter(visina_50,tezina_50,marker ="*",s=10)
plt . xlabel ('visina')
plt . ylabel ('tezina')
plt.title ('c')
plt.show ()

print(min(visina))
print(max(visina))
print(sum(visina)/len(file))

ind_m = (file[:,0] == 1)
print(file[ind_m,1].min())
print(file[ind_m,1].max())
print(file[ind_m,1].mean())

ind_z = (file[:,0] == 0)
print(file[ind_z,1].min())
print(file[ind_z,1].max())
print(file[ind_z,1].mean())