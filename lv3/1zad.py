import pandas as pd
import numpy as np

data = pd.read_csv('data_C02_emission.csv')

# a
print(len(data))
print(data.dtypes)
print(data.isnull())
print(data.duplicated())
print(data.drop_duplicates())

cat_cols = [col for col in data.columns if data[col].dtype == 'object']
for col in cat_cols:
    data[col] = data[col].astype('category')
print(data.dtypes)

# b
sorted_data = data.sort_values(by=['Fuel Consumption City (L/100km)'])
print(
    sorted_data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3))
print(
    sorted_data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].tail(3))

# c
engine = (data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)
print(data[engine])
#print(engine['CO2 Emissions (g/km)'].mean())

# d
specific_audi = data[data['Make'] == 'Audi']
audi_four = specific_audi[specific_audi['Cylinders'] == 4]
print(audi_four['CO2 Emissions (g/km)'].mean())

#e
spec_four=data[data['Cylinders']==4]
print(spec_four['CO2 Emissions (g/km)'].mean())
spec_six=data[data['Cylinders']==6]
print(spec_six['CO2 Emissions (g/km)'].mean())
spec_eight=data[data['Cylinders']==8]
print(spec_eight['CO2 Emissions (g/km)'].mean())

#f
dizel = data[data['Fuel_Type'] == 'D']['Fuel Consumption City (L/100km)'].mean()
benzin = data[data['Fuel_Type'] == 'X']['Fuel Consumption City (L/100km)'].mean()

