from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error
from sklearn import metrics
from sklearn.metrics import mean_absolute_percentage_error


data=pd.read_csv('data_C02_emission.csv')

data = data.drop(["Make", "Model"], axis = 1)

input_variables = ['Fuel Consumption City (L/100km)',
                   'Fuel Consumption Hwy (L/100km)',
                   'Fuel Consumption Comb (L/100km)',
                   'Fuel Consumption Comb (mpg)',
                   'Cylinders']

#a
output_variable = ['CO2 Emissions (g/km)']
x = data[input_variables].to_numpy()
y = data[output_variable].to_numpy()

X_train, X_test = train_test_split(x, test_size = 0.2, random_state =1)
y_train, y_test = train_test_split(y, test_size = 0.2, random_state =1)

#b
for i in range(len(input_variables)):
    train_data_x = X_train[:,i]
    train_data_y = y_train
    test_data_x = X_test[:,i]
    test_data_y = y_test
    plt.scatter(train_data_x,train_data_y, color = "blue",label = "Train")
    plt.scatter(test_data_x,test_data_y, color = "red",label = "Train")
    plt.xlabel("Input varijabla")
    plt.ylabel("C02 Emission")
    plt.show()

#c
sc = MinMaxScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)

for i in range(len(input_variables)):
    train_data_x = X_train[:, i]
    scaled_train_X = X_train_n[:,i]
    plt.hist(train_data_x, bins = 20)
    plt.show()
    plt.hist(scaled_train_X, bins = 20)
    plt.show()

#d
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print("Intercept", linearModel.intercept_)
print(linearModel.coef_)

#e
y_test_p = linearModel.predict(X_test_n)

plt.scatter(X_test["Fuel Consumption City (L/100km)"], y_test_p, color='blue', label='Training Data')
plt.scatter(X_test["Fuel Consumption City (L/100km)"], y_test, color='red', label='Testing Data')
plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel('CO2 Emissions(g/km)')
plt.legend()
plt.show()

#f
MAE = mean_absolute_error ( y_test , y_test_p )
MSE = metrics.mean_squared_error(y_test, y_test_p)
RMSE = np.sqrt(MSE)
MAPE = mean_absolute_percentage_error(y_test,y_test_p)
R2 = metrics.r2_score(y_test, y_test_p)
print(MAE,MSE,RMSE,MAPE,R2)


#g
print("50/50 split: 8.340541831221916 235.3122663008884 15.33989133927905 0.030350675795660635 0.9389869301569862")
print("80/20 split: 8.18449505021557 257.2002271349327 16.037463238771046 0.029892029481725053 0.9364108013367262")
print("20/80 split: 9.114280808885496 242.60688361821846 15.57584295048645 0.03338315399631689 0.9390570267535682")
             
